package com.ayylmaoapp.ayylmaoapp;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.os.ParcelUuid;
import android.util.Log;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.UUID;

public class BeaconService extends Service{
    IBinder mBinder = new LocalBinder();

    BluetoothAdapter mBluetoothAdapter;
    BluetoothLeScanner mBluetoothLeScanner;
    ScanFilter mScanFilter;
    ScanSettings mScanSettings;

    double ayylmao;
    double meme;

    public final static int getAyylmao = 0;
    public final static int getMemes = 1;


    public final static String ayylmaoIP = "http://172.16.2.97:3000";
    public final static String memesIP = "http://172.16.2.96:3000";

    int actual = -1;


    @Override
    public void onCreate() {
        super.onCreate();
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        mBluetoothLeScanner = mBluetoothAdapter.getBluetoothLeScanner();

        //setScanFilter();
        //setScanSettings();
        mBluetoothLeScanner.startScan(mScanCallback);

        //new OnOffTask().execute(memesIP);
        //new OnOffTask().execute(ayylmaoIP);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    //   /mode
    //  /onoff


    public class LocalBinder extends Binder {
        public BeaconService getServerInstance() {
            return BeaconService.this;
        }
    }

    public void action(int mode){
        Log.v("BeaconService", mode + "");
        new ModeATask().execute(mode + "");
        new ModeMTask().execute(mode + "");
    }

    public byte[] getIdAsByte(UUID uuid)
    {
        ByteBuffer bb = ByteBuffer.wrap(new byte[16]);
        bb.putLong(uuid.getMostSignificantBits());
        bb.putLong(uuid.getLeastSignificantBits());
        return bb.array();
    }

    private void setScanFilter() {
        ScanFilter.Builder mBuilder = new ScanFilter.Builder();
        ByteBuffer mManufacturerData = ByteBuffer.allocate(23);
        ByteBuffer mManufacturerDataMask = ByteBuffer.allocate(24);
        byte[] uuid = getIdAsByte(UUID.fromString("e2c56db5-dffb-48d2-b060-d0f5a71096e0"));
        mManufacturerData.put(0, (byte)0x4C);
        mManufacturerData.put(1, (byte)0x00);
        for (int i=2; i<=17; i++) {
            mManufacturerData.put(i, uuid[i-2]);
        }
        for (int i=0; i<=17; i++) {
            mManufacturerDataMask.put((byte)0x01);
        }
        mBuilder.setManufacturerData(161, mManufacturerData.array(), mManufacturerDataMask.array());
        mScanFilter = mBuilder.build();
    }

    private void setScanSettings() {
        ScanSettings.Builder mBuilder = new ScanSettings.Builder();
        mBuilder.setReportDelay(0);
        mBuilder.setScanMode(ScanSettings.SCAN_MODE_LOW_POWER);
        mScanSettings = mBuilder.build();
    }

    protected ScanCallback mScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            //Log.e("onScanResult", result.toString());
            //Log.e("onScanResult MAC", result.getDevice().getAddress());
            String mac = result.getDevice().getAddress();

            if (mac.equals("98:4F:EE:03:A9:EB")){
                //Log.e("Memes", result.getRssi()+"");
                meme = calculateDistance(-55, result.getRssi());
            }
            else if (mac.equals("98:4F:EE:03:A1:5D")){
                //Log.e("Ayylmao", result.getRssi()+"");
                ayylmao = calculateDistance(-55, result.getRssi());
            }

            if (meme >= ayylmao) {
                if (actual == -1){
                    actual = getMemes;
                    new OnOffTask().execute(memesIP);
                }
                else if (actual != getMemes){
                    actual = getMemes;
                    new OnOffTask().execute(memesIP);
                    new OnOffTask().execute(ayylmaoIP);
                    Log.wtf("Connected to", "ayylmao");
                }
            }
            else {
                if (actual == -1) {
                    actual = getAyylmao;
                    new OnOffTask().execute(ayylmaoIP);
                }
                else if (actual != getAyylmao){
                    actual = getAyylmao;
                    new OnOffTask().execute(memesIP);
                    new OnOffTask().execute(ayylmaoIP);
                    Log.wtf("Connected to", "meme");
                }
            }
        }
    };

    public double calculateDistance(int txPower, double rssi) {
        if (rssi == 0) {
            return -1.0; // if we cannot determine accuracy, return -1.
        }
        double ratio = rssi*1.0/txPower;
        if (ratio < 1.0) {
            return Math.pow(ratio,10);
        }
        else {
            double accuracy =  (0.89976)*Math.pow(ratio,7.7095) + 0.111;
            return accuracy;
        }
    }

    private class OnOffTask extends AsyncTask<String, Void, String> {

        HttpURLConnection urlConnection = null;

        @Override
        protected String doInBackground(String... urls) {
            // params comes from the execute() call: params[0] is the url.
            try {
                final String BASE_URL = urls[0]+"/onoff";

                Log.v("URL", BASE_URL);

                URL url = new URL(BASE_URL);

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.connect();
                Log.v("TESPONSE", urlConnection.getResponseCode()+"");
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
            return "hola";
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            // Send the results to Pebble
        }
    }

    private class ModeMTask extends AsyncTask<String, Void, String> {

        HttpURLConnection urlConnection = null;

        @Override
        protected String doInBackground(String... urls) {
            // params comes from the execute() call: params[0] is the url.
            try {

                final String BASE_URL;
                //if (actual == getAyylmao) BASE_URL = ayylmaoIP+"/mode";
                BASE_URL = memesIP+"/mode";

                Log.v("URL", BASE_URL);

                URL url = new URL(BASE_URL);

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST"); urlConnection.setRequestProperty("Content-Type", "application/json");

                String str = "{\"mode\": "+urls[0]+"}";
                byte[] outputInBytes = str.getBytes("UTF-8");
                OutputStream os = urlConnection.getOutputStream();
                os.write( outputInBytes);
                os.close();

                urlConnection.connect();
                Log.v("TESPONSE", urlConnection.getResponseCode()+"");



            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
            return "hola";
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            // Send the results to Pebble
        }
    }

    private class ModeATask extends AsyncTask<String, Void, String> {

        HttpURLConnection urlConnection = null;

        @Override
        protected String doInBackground(String... urls) {
            // params comes from the execute() call: params[0] is the url.
            try {

                final String BASE_URL;
                BASE_URL = ayylmaoIP+"/mode";
                //else BASE_URL = memesIP+"/mode";

                Log.v("URL", BASE_URL);

                URL url = new URL(BASE_URL);

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST"); urlConnection.setRequestProperty("Content-Type", "application/json");

                String str = "{\"mode\": "+urls[0]+"}";
                byte[] outputInBytes = str.getBytes("UTF-8");
                OutputStream os = urlConnection.getOutputStream();
                os.write( outputInBytes);
                os.close();

                urlConnection.connect();
                Log.v("TESPONSE", urlConnection.getResponseCode()+"");



            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
            return "hola";
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            // Send the results to Pebble
        }
    }
}
