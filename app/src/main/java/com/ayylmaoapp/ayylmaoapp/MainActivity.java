package com.ayylmaoapp.ayylmaoapp;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    boolean mBounded;
    BeaconService mServer;

    Context context;
    public static final int NETFLIX = 1;
    public static final int READ = 2;
    public static final int AUTO = 3;
    public static final int MANUAL = 4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        context = this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        findViewById(R.id.auto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpContent(AUTO);
            }
        });

        findViewById(R.id.netflix).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpContent(NETFLIX);
            }
        });

        findViewById(R.id.manual).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpContent(MANUAL);
            }
        });

        findViewById(R.id.reading).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUpContent(READ);
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent mIntent = new Intent(this, BeaconService.class);
        bindService(mIntent, mConnection, BIND_AUTO_CREATE);
    }

    ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceDisconnected(ComponentName name) {
            mBounded = false;
            mServer = null;
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
            mBounded = true;
            BeaconService.LocalBinder mLocalBinder = (BeaconService.LocalBinder)service;
            mServer = mLocalBinder.getServerInstance();
        }
    };

    @Override
    protected void onStop() {
        super.onStop();
        if(mBounded) {
            unbindService(mConnection);
            mBounded = false;
        }
    }

    private void setUpContent(int ID){
        int img;
        String title;
        String description;

        switch (ID){
            case NETFLIX:
                img = R.drawable.netflix;
                title = "NETFLIX AND CHILL";
                description = "Settle in-the simple way. One press dims the lights, silences incoming calls and turns on Netflix.";
                mServer.action(NETFLIX);
                break;
            case READ:
                img = R.drawable.read;
                title = "READING MODE";
                description = "The easiest way to get the right light intensity and temperature for a better reading experience.";
                mServer.action(READ);
                break;
            case AUTO:
                img = R.drawable.auto;
                title = "AUTO MODE";
                description = "Automatically adjust the light intensity of the bulb efficiently";
                mServer.action(AUTO);
                break;
            default:
                img = R.drawable.manual;
                title = "MANUAL MODE";
                description = "Adjust the light as you want. You have the power!";
                mServer.action(MANUAL);
        }

        ((ImageView)findViewById(R.id.main_img)).setImageResource(img);

        ((TextView)findViewById(R.id.main_title)).setText(title);
        ((TextView)findViewById(R.id.main_desc)).setText(description);

    }


}
